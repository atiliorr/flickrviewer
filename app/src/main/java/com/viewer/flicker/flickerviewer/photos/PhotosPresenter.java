package com.viewer.flicker.flickerviewer.photos;

import com.viewer.flicker.flickerviewer.model.Item;
import com.viewer.flicker.flickerviewer.model.JsonFlickrFeed;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Atilio on 30.01.18.
 */

public class PhotosPresenter {

    PhotosView mView;
    PhotosModel mModel;
    CompositeDisposable mDisposables;
    ArrayList<Item> mPhotos = new ArrayList<>();

    public PhotosPresenter(PhotosView view, PhotosModel model, CompositeDisposable disposables) {
        mView = view;
        mModel = model;
        mDisposables = disposables;
    }

    public void onCreate() {
        mDisposables.add(getPhotosList());
    }

    public void onDestroy() {
        mDisposables.clear();
    }


    private DisposableObserver<ArrayList<JsonFlickrFeed>> getPhotosList() {
        return Observable.zip(mModel.providePhotoFeed("Kittens"), mModel.providePhotoFeed("Dogs"), mModel.providePhotoFeed(""), (kittenFeed, dogFeed, publicFeed) -> {
            ArrayList<JsonFlickrFeed> jsonFlickrFeeds = new ArrayList<>();
            jsonFlickrFeeds.add(kittenFeed);
            jsonFlickrFeeds.add(dogFeed);
            jsonFlickrFeeds.add(publicFeed);
            return jsonFlickrFeeds;
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ArrayList<JsonFlickrFeed>>() {
                    @Override
                    public void onNext(ArrayList<JsonFlickrFeed> jsonFlickrFeeds) {
                        mView.updateAdapter(jsonFlickrFeeds);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
