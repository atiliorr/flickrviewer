package com.viewer.flicker.flickerviewer.photos.di;

import com.viewer.flicker.flickerviewer.di.ApplicationComponent;
import com.viewer.flicker.flickerviewer.photos.PhotosActivity;

import dagger.Component;


/**
 * Created by Atilio on 31.01.18.
 */
@PhotosScope
@Component(dependencies = {ApplicationComponent.class}, modules = {PhotosModule.class})
public interface PhotosComponent {
    void inject(PhotosActivity photosActivity);
}
