package com.viewer.flicker.flickerviewer.photo.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Atilio on 01.02.18.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
@interface PhotoScope {
}