package com.viewer.flicker.flickerviewer.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Atilio on 30.01.18.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
@interface AppScope {
}
