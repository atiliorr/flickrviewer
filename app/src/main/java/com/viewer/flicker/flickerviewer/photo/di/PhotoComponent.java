package com.viewer.flicker.flickerviewer.photo.di;

import com.viewer.flicker.flickerviewer.di.ApplicationComponent;
import com.viewer.flicker.flickerviewer.photo.PhotoActivity;

import dagger.Component;

/**
 * Created by Atilio on 01.02.18.
 */
@PhotoScope
@Component(dependencies = {ApplicationComponent.class}, modules = {PhotoModule.class})
public interface PhotoComponent {
    void inject(PhotoActivity photoActivity);
}
