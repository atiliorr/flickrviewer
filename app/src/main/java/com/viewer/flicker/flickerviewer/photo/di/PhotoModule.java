package com.viewer.flicker.flickerviewer.photo.di;

import com.viewer.flicker.flickerviewer.api.FlickrApi;
import com.viewer.flicker.flickerviewer.photo.PhotoActivity;
import com.viewer.flicker.flickerviewer.photo.PhotoModel;
import com.viewer.flicker.flickerviewer.photo.PhotoPresenter;
import com.viewer.flicker.flickerviewer.photo.PhotoView;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Atilio on 30.01.18.
 */
@Module
public class PhotoModule {

    PhotoActivity mPhotoActivity;

    public PhotoModule(PhotoActivity photoActivity) {
        this.mPhotoActivity = photoActivity;
    }

    @PhotoScope
    @Provides
    PhotoView provideView() {
        return new PhotoView(mPhotoActivity);
    }

    @PhotoScope
    @Provides
    PhotoPresenter providePresenter(PhotoView view, PhotoModel model) {
        CompositeDisposable subscriptions = new CompositeDisposable();
        return new PhotoPresenter(view, model, subscriptions);
    }

    @PhotoScope
    @Provides
    PhotoActivity provideContext() {
        return mPhotoActivity;
    }

    @PhotoScope
    @Provides
    PhotoModel provideModel(FlickrApi api) {
        return new PhotoModel(mPhotoActivity, api);
    }
}
