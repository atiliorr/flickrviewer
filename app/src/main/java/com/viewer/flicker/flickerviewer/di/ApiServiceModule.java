package com.viewer.flicker.flickerviewer.di;

import com.viewer.flicker.flickerviewer.api.FlickrApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Atilio on 30.01.18.
 */

@Module
public class ApiServiceModule {

    String mBaseUrl;

    @Provides
    public FlickrApi provideFlickrApi(Retrofit retrofit) {
        return retrofit.create(FlickrApi.class);
    }

    public ApiServiceModule(String mBaseUrl) {
        this.mBaseUrl = mBaseUrl;
    }

    @Provides
    public Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }
}
