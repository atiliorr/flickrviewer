
package com.viewer.flicker.flickerviewer.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PhotoSize {

    @SerializedName("sizes")
    private Sizes mSizes;
    @SerializedName("stat")
    private String mStat;
    @SerializedName("message")
    private String mMessage;

    public Sizes getSizes() {
        return mSizes;
    }

    public void setSizes(Sizes sizes) {
        mSizes = sizes;
    }

    public String getStat() {
        return mStat;
    }

    public void setStat(String stat) {
        mStat = stat;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
}
