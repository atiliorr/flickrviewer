package com.viewer.flicker.flickerviewer.di;

import com.viewer.flicker.flickerviewer.api.FlickrApi;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Atilio on 30.01.18.
 */

@Singleton
@Component(modules = {ApplicationModule.class, ApiServiceModule.class})
public interface ApplicationComponent {

    FlickrApi apiService();
}
