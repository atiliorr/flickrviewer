package com.viewer.flicker.flickerviewer.api;

import com.viewer.flicker.flickerviewer.model.JsonFlickrFeed;
import com.viewer.flicker.flickerviewer.model.PhotoSize;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Atilio on 30.01.18.
 */

public interface FlickrApi {

    @GET("feeds/photos_public.gne?format=json&nojsoncallback=1")
    Observable<JsonFlickrFeed> getFeed(@Query("tags") String tag);

    @GET("rest/?method=flickr.photos.getSizes&format=json&nojsoncallback=1")
    Observable<PhotoSize> getPhotoSizes(@Query("api_key") String apiKey, @Query("photo_id") String photoId);

}
