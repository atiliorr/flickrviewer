package com.viewer.flicker.flickerviewer.photo;

import com.viewer.flicker.flickerviewer.model.PhotoSize;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Atilio on 30.01.18.
 */

public class PhotoPresenter {

    PhotoView mView;
    PhotoModel mModel;
    CompositeDisposable mDisposables;

    public PhotoPresenter(PhotoView view, PhotoModel model, CompositeDisposable disposables) {
        mView = view;
        mModel = model;
        mDisposables = disposables;
    }

    public void onCreate(String key, String photoId) {
        mDisposables.add(getPhoto(key, photoId));
    }

    public void onDestroy() {
        mDisposables.clear();
    }

    private DisposableObserver<PhotoSize> getPhoto(String key, String photoId) {

        return (mModel.providePhoto(key, photoId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<PhotoSize>() {
                    @Override
                    public void onNext(PhotoSize photo) {
                        mView.updateImage(photo.getSizes().getSize().get(photo.getSizes().getSize().size() - 1).getSource());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}