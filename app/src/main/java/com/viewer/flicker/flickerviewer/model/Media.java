
package com.viewer.flicker.flickerviewer.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Media {

    @SerializedName("m")
    private String mM;

    public String getM() {
        return mM;
    }

    public void setM(String m) {
        mM = m;
    }

}
