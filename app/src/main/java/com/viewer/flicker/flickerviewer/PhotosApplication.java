package com.viewer.flicker.flickerviewer;

import android.app.Application;

import com.viewer.flicker.flickerviewer.di.ApiServiceModule;
import com.viewer.flicker.flickerviewer.di.ApplicationComponent;
import com.viewer.flicker.flickerviewer.di.ApplicationModule;
import com.viewer.flicker.flickerviewer.di.DaggerApplicationComponent;

/**
 * Created by Atilio on 30.01.18.
 */

public class PhotosApplication extends Application {

    private static ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent
                .builder()
                .apiServiceModule(new ApiServiceModule(Constants.BASE_URL))
                .applicationModule(new ApplicationModule(this)).build();
    }

    public static ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }

}
