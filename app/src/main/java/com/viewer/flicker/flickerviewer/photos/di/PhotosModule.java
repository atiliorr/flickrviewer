package com.viewer.flicker.flickerviewer.photos.di;

import com.viewer.flicker.flickerviewer.api.FlickrApi;
import com.viewer.flicker.flickerviewer.photos.PhotosActivity;
import com.viewer.flicker.flickerviewer.photos.PhotosModel;
import com.viewer.flicker.flickerviewer.photos.PhotosPresenter;
import com.viewer.flicker.flickerviewer.photos.PhotosView;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Atilio on 30.01.18.
 */
@Module
public class PhotosModule {

    PhotosActivity mPhotosActivity;

    public PhotosModule(PhotosActivity photosActivity) {
        this.mPhotosActivity = photosActivity;
    }

    @PhotosScope
    @Provides
    PhotosView provideView() {
        return new PhotosView(mPhotosActivity);
    }

    @PhotosScope
    @Provides
    PhotosPresenter providePresenter(PhotosView view, PhotosModel model) {
        CompositeDisposable subscriptions = new CompositeDisposable();
        return new PhotosPresenter(view, model, subscriptions);
    }

    @PhotosScope
    @Provides
    PhotosActivity provideContext() {
        return mPhotosActivity;
    }

    @PhotosScope
    @Provides
    PhotosModel provideModel(FlickrApi api) {
        return new PhotosModel(mPhotosActivity, api);
    }
}
