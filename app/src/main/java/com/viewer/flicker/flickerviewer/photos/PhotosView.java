package com.viewer.flicker.flickerviewer.photos;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.viewer.flicker.flickerviewer.R;
import com.viewer.flicker.flickerviewer.model.JsonFlickrFeed;
import com.viewer.flicker.flickerviewer.photos.adapter.FeedAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Atilio on 30.01.18.
 */

public class PhotosView {

    @BindView(R.id.main_feed_list)
    RecyclerView recyclerView;

    View view;
    FeedAdapter adapter;

    public PhotosView(PhotosActivity photosActivity) {
        FrameLayout parent = new FrameLayout(photosActivity);
        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(photosActivity).inflate(R.layout.activity_photos, parent, true);
        ButterKnife.bind(this, view);

        adapter = new FeedAdapter();

        recyclerView.setLayoutManager(new LinearLayoutManager(photosActivity));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
    }

    public View view() {
        return view;
    }

    public void updateAdapter(ArrayList<JsonFlickrFeed> feeds) {
        adapter.updateAdapter(feeds);
    }
}