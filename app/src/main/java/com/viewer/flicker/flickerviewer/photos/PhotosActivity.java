package com.viewer.flicker.flickerviewer.photos;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.viewer.flicker.flickerviewer.PhotosApplication;
import com.viewer.flicker.flickerviewer.R;
import com.viewer.flicker.flickerviewer.photos.di.DaggerPhotosComponent;
import com.viewer.flicker.flickerviewer.photos.di.PhotosModule;

import javax.inject.Inject;

public class PhotosActivity extends AppCompatActivity {

    @Inject
    PhotosView view;
    @Inject
    PhotosPresenter mPhotosPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);

        DaggerPhotosComponent.builder().applicationComponent(PhotosApplication.getApplicationComponent()).photosModule(new PhotosModule(this)).build().inject(this);
        setContentView(view.view());
        mPhotosPresenter.onCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPhotosPresenter.onDestroy();
    }

    public void gotoPhotoActivity(String id) {
        //Intent to detail activity
    }
}
