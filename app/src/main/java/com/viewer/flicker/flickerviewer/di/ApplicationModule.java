package com.viewer.flicker.flickerviewer.di;

import android.app.Application;

import com.viewer.flicker.flickerviewer.PhotosApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Atilio on 30.01.18.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(PhotosApplication application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mApplication;
    }
}
