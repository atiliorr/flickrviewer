
package com.viewer.flicker.flickerviewer.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Sizes {

    @SerializedName("canblog")
    private Long mCanblog;
    @SerializedName("candownload")
    private Long mCandownload;
    @SerializedName("canprint")
    private Long mCanprint;
    @SerializedName("size")
    private List<Size> mSize;

    public Long getCanblog() {
        return mCanblog;
    }

    public void setCanblog(Long canblog) {
        mCanblog = canblog;
    }

    public Long getCandownload() {
        return mCandownload;
    }

    public void setCandownload(Long candownload) {
        mCandownload = candownload;
    }

    public Long getCanprint() {
        return mCanprint;
    }

    public void setCanprint(Long canprint) {
        mCanprint = canprint;
    }

    public List<Size> getSize() {
        return mSize;
    }

    public void setSize(List<Size> size) {
        mSize = size;
    }

}
