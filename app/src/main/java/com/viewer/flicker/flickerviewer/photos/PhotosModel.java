package com.viewer.flicker.flickerviewer.photos;

import com.viewer.flicker.flickerviewer.api.FlickrApi;
import com.viewer.flicker.flickerviewer.model.JsonFlickrFeed;

import io.reactivex.Observable;

/**
 * Created by Atilio on 31.01.18.
 */

public class PhotosModel {
    PhotosActivity mPhotosActivity;
    FlickrApi mFlickrApi;

    public PhotosModel(PhotosActivity photosActivity, FlickrApi flickrApi) {
        mPhotosActivity = photosActivity;
        mFlickrApi = flickrApi;
    }

    Observable<JsonFlickrFeed> providePhotoFeed(String tag) {
        return mFlickrApi.getFeed(tag);
    }

    public void gotoPhotoActivity(String id) {
        mPhotosActivity.gotoPhotoActivity(id);
    }}
