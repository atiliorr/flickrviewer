package com.viewer.flicker.flickerviewer;

/**
 * Created by Atilio on 30.01.18.
 */

public class Constants {
    public static final String BASE_URL = "https://api.flickr.com/services/";

    public static final String ID_FLAG = "ID_FLAG";
}
