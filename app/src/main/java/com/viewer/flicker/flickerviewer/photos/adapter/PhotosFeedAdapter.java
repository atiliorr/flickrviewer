package com.viewer.flicker.flickerviewer.photos.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.viewer.flicker.flickerviewer.Constants;
import com.viewer.flicker.flickerviewer.R;
import com.viewer.flicker.flickerviewer.model.Item;
import com.viewer.flicker.flickerviewer.photo.PhotoActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Atilio on 31.01.18.
 */

public class PhotosFeedAdapter extends RecyclerView.Adapter<PhotosFeedAdapter.PhotoFeedViewHolder> {

    private List<Item> mFeedItems;
    private Context context;

    public PhotosFeedAdapter(List<Item> feedItems) {
        mFeedItems = feedItems;
    }

    @Override
    public PhotosFeedAdapter.PhotoFeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_feed_item, parent, false);
        context = parent.getContext();
        return new PhotosFeedAdapter.PhotoFeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotosFeedAdapter.PhotoFeedViewHolder holder, int position) {
        Item feedItem = mFeedItems.get(position);

        Glide.with(context)
                .load(feedItem.getMedia().getM())
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(holder.mFeedItemPhoto);

        holder.mFeedItemTitle.setText(feedItem.getTitle());

        String photo = feedItem.getMedia().getM();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PhotoActivity.class);
                intent.putExtra(Constants.ID_FLAG, photo.substring((photo.lastIndexOf("/") + 1), photo.indexOf("_")));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mFeedItems != null && mFeedItems.size() > 0) {
            return mFeedItems.size();
        } else {
            return 0;
        }
    }

    public class PhotoFeedViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.feed_item_photo_title)
        TextView mFeedItemTitle;

        @BindView(R.id.feed_item_photo)
        ImageView mFeedItemPhoto;

        public PhotoFeedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
