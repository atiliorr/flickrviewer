package com.viewer.flicker.flickerviewer.photo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.viewer.flicker.flickerviewer.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Atilio on 30.01.18.
 */

public class PhotoView {

    @BindView(R.id.photo)
    ImageView mPhoto;

    PhotoActivity mPhotoActivity;

    View view;

    public PhotoView(PhotoActivity photosActivity) {
        mPhotoActivity = photosActivity;
        FrameLayout parent = new FrameLayout(photosActivity);
        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(photosActivity).inflate(R.layout.activity_photo, parent, true);
        ButterKnife.bind(this, view);
    }

    public View view() {
        return view;
    }

    public void updateImage(String imageUrl) {
        Glide.with(mPhotoActivity)
                .load(imageUrl)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(mPhoto);
    }
}
