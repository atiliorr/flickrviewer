package com.viewer.flicker.flickerviewer.photo;

import com.viewer.flicker.flickerviewer.api.FlickrApi;
import com.viewer.flicker.flickerviewer.model.PhotoSize;

import io.reactivex.Observable;

/**
 * Created by Atilio on 01.02.18.
 */

public class PhotoModel {
    PhotoActivity mPhotoActivity;
    FlickrApi mFlickrApi;

    public PhotoModel(PhotoActivity photosActivity, FlickrApi flickrApi) {
        mPhotoActivity = photosActivity;
        mFlickrApi = flickrApi;
    }

    Observable<PhotoSize> providePhoto(String key, String photoId) {
        return mFlickrApi.getPhotoSizes(key, photoId);
    }
}
