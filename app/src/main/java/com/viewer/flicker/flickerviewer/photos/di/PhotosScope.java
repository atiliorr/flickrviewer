package com.viewer.flicker.flickerviewer.photos.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Atilio on 31.01.18.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
@interface PhotosScope {
}
