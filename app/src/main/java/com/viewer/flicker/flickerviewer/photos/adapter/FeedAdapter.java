package com.viewer.flicker.flickerviewer.photos.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viewer.flicker.flickerviewer.R;
import com.viewer.flicker.flickerviewer.model.JsonFlickrFeed;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Atilio on 31.01.18.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedViewHolder> {

    private ArrayList<JsonFlickrFeed> mFeeds = new ArrayList<>();
    private PhotosFeedAdapter mAdapter;
    private Context mContext;

    public void updateAdapter(ArrayList<JsonFlickrFeed> items) {
        this.mFeeds.clear();
        this.mFeeds.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public FeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_item, parent, false);
        mContext = parent.getContext();
        return new FeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeedViewHolder holder, int position) {
        JsonFlickrFeed feed = mFeeds.get(position);

        holder.feedName.setText(feed.getTitle());

        mAdapter = new PhotosFeedAdapter(feed.getItems());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        holder.mPhotoList.setLayoutManager(linearLayoutManager);
        holder.mPhotoList.setHasFixedSize(true);
        holder.mPhotoList.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        if (mFeeds != null && mFeeds.size() > 0) {
            return mFeeds.size();
        } else {
            return 0;
        }
    }

    public class FeedViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.feed_tag)
        TextView feedName;

        @BindView(R.id.feed_list)
        RecyclerView mPhotoList;

        public FeedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
