package com.viewer.flicker.flickerviewer.photo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.viewer.flicker.flickerviewer.PhotosApplication;
import com.viewer.flicker.flickerviewer.R ;
import com.viewer.flicker.flickerviewer.photo.di.DaggerPhotoComponent;
import com.viewer.flicker.flickerviewer.photo.di.PhotoModule;

import javax.inject.Inject;

public class PhotoActivity extends AppCompatActivity {

    @Inject
    PhotoView view;
    @Inject
    PhotoPresenter mPhotoPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        DaggerPhotoComponent.builder().applicationComponent(PhotosApplication.getApplicationComponent()).photoModule(new PhotoModule(this)).build().inject(this);

        setContentView(view.view());
        mPhotoPresenter.onCreate(getString(R.string.APIKey), getIntent().getStringExtra("ID_FLAG"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPhotoPresenter.onDestroy();
    }

}
